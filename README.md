# Projeto de Máquina de Estados em C

Autor: Prof. Dr. Marcos Barreto

Adaptado por: Prof. Dr. André Kubagawa Sato

Implementação em linguagem C da máquina de estados do projeto de alarme residencial

## Diagrama de Estados

<!--
```graphviz
digraph finite_state_machine {

    node [shape = Mrecord]; Espera, Saida, Alerta, Entrada, Acionado;
    node [shape = point];
    Inicio -> Espera;

    Espera  -> Saida [ label = "acionar / A01" ];
    Saida  -> Alerta [ label = "timeout / A02" ];
    Alerta  -> Entrada [ label = "disparar / A05" ];

    Entrada  -> Acionado [ label = "timeout / A06" ];
    Acionado  -> Espera [ label = "desacionar / A07" ];
    Saida  -> Espera [ label = "desacionar / A03" ];
    Alerta  -> Espera [ label = "desacionar / A04" ];
    Entrada  -> Espera [ label = "desacionar / A07" ];
}
```
-->

![Menu de configurações avançadas de ambiente](state_machine.svg)

Onde as ações são:

| Nome | Ações                                                                           |
| ---- | ------------------------------------------------------------------------------- |
| A01  | - iniciar temporização                                                          |
| A02  | - acionar sirene <br> - avisar central (acionamento)                            |
| A03  | - avisar central (desacionamento) <br> - parar temporização                     |
| A04  | - avisar central (desacionamento)                                               |
| A05  | - iniciar temporização                                                          |
| A06  | - disparar sirene <br> - avisar central (invasão)                               |
| A07  | - avisar central (desacionamento) <br> - parar temporização <br> - parar sirene |

## Fazendo o _Build_ do projeto no Windows

```bash
$ mingw32-make build
```

## Fazendo o _Build_ do projeto no Windows

```bash
$ make build
```

## Limpando os arquivos de _Build_ no Windows

```bash
$ mingw32-make clean
```

## Limpando os arquivos de _Build_ no Linux

Primeiro, altere o arquivo `Makefile` para

```Makefile=
...

.PHONY: clean
clean:
#	For non Windows users:
    rm -f $(OBJS) $(TARGET)
```

Importante: o Makefile usa tabulação (não espaços).

Por fim, o comando para a limpeza é

```bash
$ make clean
```
