#include <stdio.h>
#include <stdlib.h>

#include "definicoes_sistema.h"
#include "sensores.h"

/************************
 snr_estaAtivo
 Retorna se existe algum sensor ativo
 entradas
   nenhuma
 saidas
   numero do sensor ativo ou NENHUM_SENSOR_ATIVO
*************************/
int snr_estaAtivo()
{
    return NENHUM_SENSOR_ATIVO;
}
