#include <stdio.h>
#include <stdlib.h>

/*
    ALARME RESIDENCIAL
*/

#include "definicoes_sistema.h"
#include "comunicacao.h"
#include "ihm.h"
#include "senhas.h"
#include "sensores.h"
#include "sirene.h"
#include "timer.h"
#include "zonas.h"

/***********************************************************************
 Estaticos
 ***********************************************************************/
  int codigoEvento;
  int codigoAcao;
  int estado;
  int sensores;
  int acao_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];
  int proximo_estado_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];

/************************************************************************
 executarAcao
 Executa uma acao
 Parametros de entrada:
    (int) codigo da acao a ser executada
 Retorno: (int) codigo do evento interno ou NENHUM_EVENTO
*************************************************************************/
int executarAcao(int codigoAcao)
{
    int retval;

    retval = NENHUM_EVENTO;
    if (codigoAcao == NENHUMA_ACAO)
        return retval;

    switch(codigoAcao)
    {
    case A01:
        tmr_iniciar(true);
        break;
    case A02:
        sne_bip();
        com_notificar("Alarme em alerta");
        tmr_iniciar(false);
        break;
    case A03:
        com_notificar("Alarme desacionado");
        tmr_iniciar(false);
        break;
    case A04:
        com_notificar("Alarme desacionado");
        break;
    case A05:
        tmr_iniciar(true);
        break;
    case A06:
        sne_acionar(true);
        com_notificar("Invasao");
        tmr_iniciar(false);
        break;
    case A07:
        com_notificar("Alarme desacionado");
        tmr_iniciar(false);
        sne_acionar(false);
        break;
    } // switch

    return retval;
} // executarAcao

/************************************************************************
 iniciaMaquina de Estados
 Carrega a maquina de estados
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
void iniciaMaquinaEstados()
{
  int i;
  int j;

  for (i=0; i < NUM_ESTADOS; i++) {
    for (j=0; j < NUM_EVENTOS; j++) {
       acao_matrizTransicaoEstados[i][j] = NENHUMA_ACAO;
       proximo_estado_matrizTransicaoEstados[i][j] = i;
    }
  }
  proximo_estado_matrizTransicaoEstados[ESPERA][ACIONAR] = SAIDA;
  acao_matrizTransicaoEstados[ESPERA][ACIONAR] = A01;

  proximo_estado_matrizTransicaoEstados[SAIDA][DESACIONAR] = ESPERA;
  acao_matrizTransicaoEstados[SAIDA][DESACIONAR] = A03;

  proximo_estado_matrizTransicaoEstados[SAIDA][TIMEOUT] = ALERTA;
  acao_matrizTransicaoEstados[SAIDA][TIMEOUT] = A02;

  proximo_estado_matrizTransicaoEstados[ALERTA][DESACIONAR] = ESPERA;
  acao_matrizTransicaoEstados[ALERTA][DESACIONAR] = A04;

  proximo_estado_matrizTransicaoEstados[ALERTA][DISPARAR] = ENTRADA;
  acao_matrizTransicaoEstados[ALERTA][DISPARAR] = A05;

  proximo_estado_matrizTransicaoEstados[ENTRADA][TIMEOUT] = ACIONADO;
  acao_matrizTransicaoEstados[ENTRADA][TIMEOUT] = A06;

  proximo_estado_matrizTransicaoEstados[ENTRADA][DESACIONAR] = ESPERA;
  acao_matrizTransicaoEstados[ENTRADA][DESACIONAR] = A07;

  proximo_estado_matrizTransicaoEstados[ACIONADO][DESACIONAR] = ESPERA;
  acao_matrizTransicaoEstados[ACIONADO][DESACIONAR] = A07;


} // initStateMachine

/************************************************************************
 iniciaSistema
 Inicia o sistema ...
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
void iniciaSistema()
{
   iniciaMaquinaEstados();
} // initSystem


/************************************************************************
 obterEvento
 Obtem um evento, que pode ser da IHM ou do alarme
 Parametros de entrada: nenhum
 Retorno: codigo do evento
*************************************************************************/
char* teclas;

int decodificarAcionar()
{
    if (teclas[2] == 'a')
    {
        if (sha_validar(teclas))
        {
            return true;
        }
    }
    return false;
}//decodificarAcionar

int decodificarDesacionar()
{
    if (teclas[2] == 'd')
    {
        if (sha_validar(teclas))
        {
            return true;
        }
    }
    return false;
}//decodificarDesacionar

int decodificarDisparar()
{
    if (teclas[0] == 'l')
    {
        return true;
    }
    return false;
}//decodificarDisparar

int decodificarTimeout()
{
    return tmr_timeout();
}

int obterEvento()
{
  int retval = NENHUM_EVENTO;

  teclas = ihm_obterTeclas();
  if (decodificarAcionar())
    return ACIONAR;
  if (decodificarDesacionar())
    return DESACIONAR;
  if (decodificarTimeout())
    return TIMEOUT;
  if (decodificarDisparar())
    return DISPARAR;

  return retval;

} // obterEvento

/************************************************************************
 obterAcao
 Obtem uma acao da Matriz de transicao de estados
 Parametros de entrada: estado (int)
                        evento (int)
 Retorno: codigo da acao
*************************************************************************/
int obterAcao(int estado, int codigoEvento) {
  return acao_matrizTransicaoEstados[estado][codigoEvento];
} // obterAcao


/************************************************************************
 obterProximoEstado
 Obtem o proximo estado da Matriz de transicao de estados
 Parametros de entrada: estado (int)
                        evento (int)
 Retorno: codigo do estado
*************************************************************************/
int obterProximoEstado(int estado, int codigoEvento) {
  return proximo_estado_matrizTransicaoEstados[estado][codigoEvento];
} // obterAcao


/************************************************************************
 Main
 Loop principal de controle que executa a maquina de estados
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
int main() {

  int codigoEvento;
  int codigoAcao;
  int estado;
  int eventoInterno;

  estado = ESPERA;
  eventoInterno = NENHUM_EVENTO;

  iniciaSistema();
  printf ("Alarme iniciado\n");
  while (true) {
    if (eventoInterno == NENHUM_EVENTO) {
        codigoEvento = obterEvento();
    } else {
        codigoEvento = eventoInterno;
    }
    if (codigoEvento != NENHUM_EVENTO)
    {
       codigoAcao = obterAcao(estado, codigoEvento);
       estado = obterProximoEstado(estado, codigoEvento);
       eventoInterno = executarAcao(codigoAcao);
       printf("Estado: %d Evento: %d Acao:%d\n", estado, codigoEvento, codigoAcao);
    }
  } // while true
} // main
