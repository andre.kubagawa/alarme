#ifndef SENSORES_H_INCLUDED
#define SENSORES_H_INCLUDED

#define NENHUM_SENSOR_ATIVO -1

/************************
 snr_estaAtivo
 Retorna se existe algum sensor ativo
 entradas
   nenhuma
 saidas
   numero do sensor ativo ou NENHUM_SENSOR_ATIVO
*************************/
extern int snr_estaAtivo();


#endif // SENSORES_H_INCLUDED
