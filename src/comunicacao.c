#include <stdio.h>
#include <stdlib.h>

#include "definicoes_sistema.h"
#include "comunicacao.h"

/************************
 com_notificar
 Envia mensagem para a Central
 entradas
   texto : texto para envio para Central
 saidas
   nenhuma
*************************/
void com_notificar(char* texto)
{
    printf("Comunicacao com a Central: %s\n", texto);
}
