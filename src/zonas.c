#include <stdio.h>
#include <stdlib.h>

#include "definicoes_sistema.h"
#include "zonas.h"

#define NUM_ZONAS 3
#define NUM_SENSORES 3

// determina se uma zona esta ou nao ativa
int situacaoZona [NUM_ZONAS];

// determina quais sensores pertencem a qual zona
int sensorZona [NUM_SENSORES];

/************************
 zna_habilitar
 Habilita ou desabilita zona
 entradas
   zona : numero da zona
   controle: TRUE:habilita FALSE:desabilita
 saidas
   nenhuma
*************************/
void zna_habilitar(int zona, int controle)
{

}


/************************
 zna_estaHabilidada
 Retorna a situacao da zona aa qual pertence o sensor informado
 entradas
   zona : numero do sensor
 saidas
   TRUE:habilitada
   FALSE:desabilitada
*************************/
int zna_estaHabilitada(int sensor)
{
   return true;
}
