#ifndef ZONAS_H_INCLUDED
#define ZONAS_H_INCLUDED


/************************
 zna_habilitar
 Habilita ou desabilita zona
 entradas
   zona : numero da zona
   controle: TRUE:habilita FALSE:desabilita
 saidas
   nenhuma
*************************/
extern void zna_habilitar(int zona, int controle);


/************************
 zna_estaHabilidada
 Retorna a situacao da zona aa qual pertence o sensor informado
 entradas
   zona : numero do sensor
 saidas
   TRUE:habilitada
   FALSE:desabilitada
*************************/
extern int zna_estaHabilitada(int sensor);


#endif // ZONAS_H_INCLUDED
