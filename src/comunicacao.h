#ifndef COMUNICACAO_H_INCLUDED
#define COMUNICACAO_H_INCLUDED

/************************
 com_notificar
 Envia mensagem para a Central
 entradas
   texto : texto para envio para Central
 saidas
   nenhuma
*************************/
extern void com_notificar(char* texto);

#endif // COMUNICACAO_H_INCLUDED
